let http = require("http")

let port = 1001

const serv = http.createServer((request, response) => {

	if (request.url == '/login'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to the login page.")

	} else if (request.url == '/register') {

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found')
	}
})

serv.listen(port);

console.log(`Server now accessible at localhost:${port}.`)
